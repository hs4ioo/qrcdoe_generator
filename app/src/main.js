import Vue from 'vue'
import App from './App.vue'

//css and font awesom 
import 'bulma/css/bulma.min.css'      

import { library } from '@fortawesome/fontawesome-svg-core'
import { faUser, faCoffee, faAt } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'


library.add(faUser, faCoffee, faAt ) // แสดง icon User หากจะเพิ่มให้ใส่ , เพิ่มไปได้เรื่อย ๆ อย่าลืมเพิ่ม ตรง object ด้านบนด้วย 
Vue.component('font-awesome-icon', FontAwesomeIcon)

// close font 

import router from '@/router'


Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  router
}).$mount('#app')
