import Vue from 'vue'
import VueRouter from 'vue-router'

import Index from '@/views/Index'
import Login from '@/views/Login'
import Trader from '@/views/Trader'
import Qrcode from '@/views/Qrcode'

Vue.use(VueRouter)

const routes = [
    {
        path: '/', component: Index
    },
    {
        path: '/login', component: Login
    },
    {
        path: '/trader', component: Trader
    },
    {
        path: '/QRCODE', component: Qrcode
    }
]

const router = new VueRouter({
    mode: 'history',
    routes
})

export default router