var express = require("express")
var cors = require("cors")
var bodyPaser = require("body-parser")
var app = express()
var port = process.env.port || 5000

app.use(bodyPaser.json())
app.use(cors())
app.use(bodyPaser.urlencoded({ extended: false }))


app.listen(port, () => {
    console.log("Server is running on port" + port)
})
