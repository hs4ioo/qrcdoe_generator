const Sequelize = require('sequelize')
const db = require('../database/db.js')

module.export = db.sequelize.define(
    'trader',
    {
        trader_id: {
            type: Sequelize.STRING
        },
        trader_password: {
            type: Sequelize.STRING
        }
    },
    {
        timestamps: false
    }
);