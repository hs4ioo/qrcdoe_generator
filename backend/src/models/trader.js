const Sequelize = require('sequelize')
const sequelize = require('../setup-sequelize')

const traders = {
    trader_autonumber: {
        primaryKey: true,
        autoIncrement: true,
        type: Sequelize.BIGINT 
    },
    trader_id: Sequelize.STRING,
    trader_password: Sequelize.STRING,
    qr_token: Sequelize.STRING
}

const options = {
    timestamps: false,
    freezeTableName: true
}

module.exports = sequelize.define('btest_tb10_nsw_lite_trader', traders, options)

