const router = require('express').Router()
const bcrypt = require('bcryptjs')

const User = require('../models/trader')

const cors = require('cors')
router.use(cors())

router.get('/:trader_id', async (req, res) => {
    const { trader_id } = req.params

    if (!trader_id) {
        res.status(500).end()
        return
      
    }

    try {//
        // query the database using primary key
        // const user = await User.findByPk(id) ของเดิมอาจารย์จืดทำไว้
        const user = await User.findOne({
            where: {
                qr_token: trader_id   // ให้ตรวจสอบ จาก token แทน ทำ token ขึ้นมาใหม่ เพื่อ ใช้ gen qrcode 
            }
        })

        if (!user) {
            res.status(404).end()
           
        } else {
            res.json(user)
        }
    } catch (error) {
        res.status(500).end()
    }
})

module.exports = router