const fs = require('fs')
const path = require('path')
const router = require('express').Router()
const jwt = require('jsonwebtoken')
const Trader = require('../models/trader')
const cors = require('cors')

const privateKey = fs.readFileSync(path.resolve('certs','private.pem'))

router.use(cors())

router.post('/login', async (req, res) => {
    const { trader_id, trader_password } = req.body
    try {
        if (!trader_id || !trader_password) {
            res.status(500).end()
            
            return
        }

        const trader = await Trader.findOne({
            where: {
                trader_id,
                trader_password
            }
            
        })
        

        if (!trader) {
            res.status(500).end()

            return
        }
        //console.log(trader)
        const remember_token = trader.remember_token // ดึงจาก db
        

        const payload = {
            trader_id,
            role: trader['role']
        }

        const token = jwt.sign(payload, privateKey, { algorithm: 'RS256' })
        res.json({ token, trader_id,trader_password,remember_token}) // ส่งค่า json กลับไป ด้วย หากจะส่งบาง fields ก็ ส่ง เฉพาะ fields
        
    } catch (error){
        res.status(500).end()
    }
})

module.exports = router